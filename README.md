# Fmea-mcdm

**Implementação de Software para aplicação de Métodos de Decisão Multicritério (MCDM) em conjunto da Analise de Efeitos e Modos de Falha (FMEA).**


Neste software pretende-se implementar (algoritmos) MCDM indicados na literatura que recomendam decisões levando em consideração preferências e muitos critérios. 
Neste caso, as decisões seriam a respeito de formas de solucionar falhas ou defeitos de processos industriais identificados pelo FMEA. 
Além disso, espera-se contribuir neste projeto a inclusão de uma interface gráfica (GUI) apropriada para execução dos métodos de acordo com 
arquivos .CSV introduzido pelo 'cliente'.

A seguir uma check-list de MCDM que serão implementados: (os marcados já estão implementados)

MCDM:
- [x] TOPSIS
- [x] GRA
- [ ] AHP
- [ ] ELECTRE
- [ ] MACBETH
- [ ] VIKOR

Como o intuito é o de fornecer este software de forma livre para uso e aceitar pull-requests, ou seja, aceitar contribuições de outros pesquisadores, tal que,
pretende-se disponibilizar uma ferramenta para agilizar e mitigar o trabalho que pesquisadores e gestores têm atualmente em planilhas eletrônicas, 
ou softwares que contém a implementação de apenas um MCDM.

Modelagens em execução:
- GUI
- Arquivo .CSV de entrada FMEA (o que é necessário? Como pedir/receber do cliente essa matriz)
- Gerar Arquivo FMEA normalizado(pronto para MCDM)



