import sys
sys.path.append('../')

from utils.normalizer import Normalizer
from topsis import TOPSIS
import numpy as np


class Main:

    # matrix = np.array(pd.read_csv('../../data/teste.csv', header=None, decimal='.'))
    matrix = np.array(
        [[144990.00, 6.2, 11.20, 6.80, 37.00, 510.00, 3.00],
        [145427.00, 7.7, 11.30, 7.30, 30.00, 480.00, 5.00],
        [174990.00, 6.0, 11.55, 6.90, 38.00, 425.00, 4.00],
        [98900.00, 8.3, 11.75, 10.90, 25.00, 519.00, 2.00]]
    )
    weight = np.array([0.2, 0.1, 0.4, 0.1, 0.1, 0.05, 0.05])
    attr = np.array([0, 0, 0, 0, 1, 1, 1])


    def test_norm(self, norm):
        if norm == 1:
            return np.linalg.norm(Normalizer.norm1(Normalizer, self.matrix, self.attr))
        elif norm == 2:
            return np.linalg.norm(Normalizer.norm2(Normalizer, self.matrix, self.attr))
        elif norm == 3:
            return np.linalg.norm(Normalizer.norm3(Normalizer, self.matrix, self.attr))
        elif norm == 4:
            print(Normalizer.norm4(Normalizer, self.matrix, self.attr))
            return np.linalg.norm(Normalizer.norm4(Normalizer, self.matrix, self.attr))


    def test_topsis(self, TOPSIS):
        method = TOPSIS(self.matrix, self.weight, self.attr)
        method.topsis_method()

        return f'{method.P} \n\n {method.proximity} \n\n {method.get_great_proximity()}'
        

    def __str__(self):
        return f'{self.test_topsis(TOPSIS)}'


test = Main()
print(test.test_norm(4))
