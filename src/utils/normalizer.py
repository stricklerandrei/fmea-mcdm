import numpy as np
from math import sqrt


class Normalizer:

    def norm1(self, matrix, attr):

        norm_matrix = np.copy(matrix)
        norm_matrix = np.array(norm_matrix, dtype=np.float64)

        max_value = self.getMax(self, matrix)
        min_value = self.getMin(self, matrix)

        # print(f'min: {min_value}, max: {max_value}')

        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if attr[j] == 1:
                    norm_matrix[i, j] = float(matrix[i, j]/max_value)
                elif attr[j] == 0:
                    norm_matrix[i, j] = float(min_value/matrix[i, j])                        

        return norm_matrix


    def norm2(self, matrix, attr):
        norm_matrix = np.copy(matrix)
        norm_matrix = np.array(norm_matrix, dtype=np.float64)

        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if attr[j] == 1:
                    norm_matrix[i, j] = float(matrix[i, j]/self.getSum(self, matrix))
                elif attr[j] == 0:
                    norm_matrix[i, j] = float((1/matrix[i, j])/self.getInverseSum(self, matrix))

        return norm_matrix


    def norm3(self, matrix, attr):
        norm_matrix = np.copy(matrix)
        norm_matrix = np.array(norm_matrix, dtype=np.float64)

        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if attr[j] == 1:
                    norm_matrix[i, j] = float(matrix[i, j]/self.getSquaredSum(self, matrix))
                elif attr[j] == 0:
                    norm_matrix[i, j] = float((1/matrix[i, j])/self.getInverseSquaredSum(self, matrix))

        return norm_matrix


    def norm4(self, matrix, attr):
        norm_matrix = np.copy(matrix)
        norm_matrix = np.array(norm_matrix, dtype=np.float64)

        max_value = self.getMax(self, matrix)
        min_value = self.getMin(self, matrix)

        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if attr[j] == 1:
                    norm_matrix[i, j] = float((matrix[i, j] - min_value)/(max_value - min_value))
                elif attr[j] == 0:
                    norm_matrix[i, j] = float((max_value - matrix[i, j])/(max_value - min_value))

        return norm_matrix


    def getMax(self, matrix):

        max_value = 0

        for i in range(len(matrix)):
            temp = max(matrix[i])

            if temp > max_value:
                max_value = temp

        return max_value


    def getMin(self, matrix):

        min_value = self.getMax(self, matrix)

        for i in range(len(matrix)):
            temp = min(matrix[i])

            if temp < min_value:
                max_value = temp

        return max_value


    def getSquaredSum(self, matrix):
        _sum = 0

        for i in range(len(matrix)):
            _sum = sum(matrix[i]**2)

        return sqrt(_sum)


    def getInverseSquaredSum(self, matrix):
        _sum = 0

        for i in range(len(matrix)):
            _sum = sum((matrix[i]**(-1))**2)

        return sqrt(_sum)

    
    def getSum(self, matrix):
        _sum = 0

        for i in range(len(matrix)):
            _sum = sum(matrix[i])

        return sqrt(_sum)


    def getInverseSum(self, matrix):
        _sum = 0

        for i in range(len(matrix)):
            _sum = sum(matrix[i]**(-1))

        return sqrt(_sum)